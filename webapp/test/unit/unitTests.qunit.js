QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "iba/gomel/by/myshop/test/unit/model/formatter"
    ], function () {
        QUnit.start();
    });
});

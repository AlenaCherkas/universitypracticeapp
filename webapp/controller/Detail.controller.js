sap.ui.define([
    "sap/m/MessageToast",
    "./BaseController",
    "sap/ui/model/resource/ResourceModel",
    "sap/f/LayoutType",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (MessageToast, BaseController, ResourceModel, LayoutType, JSONModel, Filter, FilterOperator) {
    "use strict";
    return BaseController.extend("iba.gomel.by.myshop.controller.Detail", {
        
        _onObjectMatched: function (oEvent) {
            var oDataModel = this.getODataModel();
            var oController = this;
            oDataModel.read("/" + window.decodeURIComponent(oEvent.getParameter("arguments").detail), {
                success: function (oData, oResponse) {
                    var oModel = new JSONModel();
                    oModel.setData(oData);
                    this.setCharacteristics(oData);
                    this.setModel(oModel, "oDetail");
                }.bind(this),
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });

            this._oFlexibleColumnLayout.setLayout(LayoutType.TwoColumnsBeginExpanded);
            var oParams = {
                sExpandParameters : "Currency",
                sModelName : "personOdata",
                bIsDeepInto : false               
            }
            this.setUserModel(oParams);
        },

        onInit: function () {
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
            this._oFlexibleColumnLayout = this.getOwnerComponent().getRootControl().byId("fcl");
        },

        setCharacteristics: function (oData) {
            var oCharacteristics = {};
            oCharacteristics.main = [];
            oCharacteristics.specifications = [];

            var iSpecificationsLength = oData.Specifications.length;
            for (var i = 0; i < iSpecificationsLength; i++) {
                var aSpecification = oData.Specifications[i];
                var oCharacteristic = {
                    "name": aSpecification[0],
                    "value": aSpecification[1]
                };
                oCharacteristics.specifications.push(oCharacteristic);
            }

            var iMainCharacteristicsLength = oData.MainCharacteristics.length;
            for (var i = 0; i < iMainCharacteristicsLength; i++) {
                var aMainCharacteristic = oData.MainCharacteristics[i];
                var oMainCharacteristic = {
                    "name": aMainCharacteristic[0],
                    "value": aMainCharacteristic[1]
                };
                oCharacteristics.main.push(oMainCharacteristic);
            }

            var oModel = new JSONModel();
            oModel.setData(oCharacteristics);
            this.setModel(oModel, "oCharacteristics");
        },

        onCloseColumn: function (oEvent) {
            this._oF_oFlexibleColumnLayoutcl.setLayout(LayoutType.OneColumn);
            this.byId("fullScrean").setIcon("sap-icon://full-screen"); 
        },

        onFullScrean: function (oEvent) {
            if (this._oFlexibleColumnLayout.getLayout() != LayoutType.TwoColumnsBeginExpanded) {
                this._oFlexibleColumnLayout.setLayout(LayoutType.TwoColumnsBeginExpanded);
                oEvent.getSource().setIcon("sap-icon://full-screen"); 
            } else {
                this._oFlexibleColumnLayout.setLayout(LayoutType.MidColumnFullScreen);
                oEvent.getSource().setIcon("sap-icon://exit-full-screen");   
            }
        },

        addToCard: function(oEvent) {
            var oDetailModel = this.getModelByName("oDetail").oData;
            var oDataModel = this.getODataModel();
            var oController = this;

            oDataModel.read("/CardDetails", {
                filters: [
                    new Filter({
                        path: "DetailID",
                        operator: FilterOperator.EQ,
                        value1: oDetailModel.ID
                    }),
                    new Filter({
                        path: "CardID",
                        operator: FilterOperator.EQ,
                        value1: oController.getModelProperty("personOdata", "/CardID")
                    })
                ],
                success: function(oData, oResponse) {
                    if (oData.results.length === 0) {
                        var oNewCardDetail = {
                            "CardID": oController.getModelProperty("personOdata", "/CardID"),
                            "DetailID": oDetailModel.ID,
                            "Count": 1,
                            "IsBuyNow": true
                        };
                        oController.createEntity(oDataModel, "CardDetails", oNewCardDetail, "productHasBeenAdded");
                    } else {
                            var oCardDetail = oData.results[0];
                            oDataModel.read("/StockInfos", {
                                filters: [
                                    new Filter({
                                        path: "ID",
                                        operator: FilterOperator.EQ,
                                        value1: oDetailModel.StockinfoID
                                    })
                                ],
                                success: function(oData, oResponse) {
                                    var oStockInfo = oData.results[0];
                                    if (oStockInfo.Count >= (1 + oCardDetail.Count)) {
                                        oCardDetail.Count = oCardDetail.Count + 1;
                                        oController.updateEntity(oDataModel, "CardDetails", oCardDetail, "productHasBeenAdded");
                                    } else {
                                        MessageToast.show(oController.getBundle().getText("maxQuantity"));
                                    }
                                },
                                error: function(oErr) {
                                    oController.showErrorPopupMessage(oErr.message);
                                }
                            })
                        }
                    },
                    error: function(oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                    }
                });
        },

        addToChosen: function(oEvent) {
            var oDetailModel = this.getModelByName("oDetail").oData;
            var oDataModel = this.getODataModel();
            var oController = this;
            oDataModel.read("/ChosenDetails", {
                filters: [
                    new Filter({
                        path: "DetailID",
                        operator: FilterOperator.EQ,
                        value1: oDetailModel.ID
                    }),
                    new Filter({
                        path: "CardID",
                        operator: FilterOperator.EQ,
                        value1: oController.getModelProperty("personOdata", "/CardID")
                    })
                ],
                success: function(oData, oResponse) {
                    if (oData.results.length === 0) {
                        var oNewChosenDetail = {
                            "CardID": oController.getModelProperty("personOdata", "/CardID"),
                            "DetailID": oDetailModel.ID
                        }
                        oController.createEntity(oDataModel, "ChosenDetails", oNewChosenDetail, "productHasBeenAddedChosen");
                    } else {
                            MessageToast.show(oController.getBundle().getText("productHasBeenAddedToChosenBefore"));
                        }
                },
                error: function(oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        }
    });
});

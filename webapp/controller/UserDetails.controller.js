sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast"
], function (BaseController, JSONModel, MessageToast) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.UserDetails", {

        _setUserModel: function(oEvent) {
            var oParams = {
                sExpandParameters : "Currency",
                sModelName : "personOdata",
                bIsDeepInto : false               
            }
            this.setUserModel(oParams);
        },

        _onObjectMatched: function(oEvent) {
            this.onChangeEditable(false);
            this._setUserModel();
            this.setCurrencies();
        },

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("userinfo").attachPatternMatched(this._onObjectMatched, this);

            var oData = {
                editConfig : {
                    isEditModeOn : false
                }
            };
            var oConfigModel = new JSONModel(oData);
            this.getView().setModel(oConfigModel, "configModel");
        },

        onPressPrimaryOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("primaryPage"));		
        },

        onPressContactOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("contactPage"));		
        },

        onPressDeliveryOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("deliveryPage"));		
        },

        onPressPrivateOption: function(oEvent) {
            this.getSplitAppObj().to(this.createId("privatePage"));	
            this._setUserModel();
            this.byId("currencyCB").setSelectedKey(this.getModelByName("personOdata").getData().Currency.ID);
        },

        getSplitAppObj: function() {
            var oSplitApp = this.byId("SplitAppDemo");
            return oSplitApp;
        },

        onReject: function(oEvent) {
            this._setUserModel();
            this.byId("currencyCB").setSelectedKey(this.getModelByName("personOdata").getData().Currency.ID);
            this.onChangeEditable(false);
        },

        onAccept: function(oEvent) {
            var oController = this;
            var oUserModel = this.getModelByName("personOdata").getData();
            var oUser = this.copyObject(oUserModel);
            oUser.CurrencyID = parseInt(this.byId("currencyCB").getSelectedKey());                 
            var oDataModel = this.getODataModel();

            oController.updateEntity(oDataModel, "Users", oUser, "userInfoHaveBeenChanged");
            oController._onObjectMatched();
        },

        onEdit: function(oEvent) {
            this.onChangeEditable(oEvent.getSource().getVisible());
        },

        onChangeEditable: function(bState) {
            var oEditConfig = this.getModelByName("configModel");
            oEditConfig.setProperty("/editConfig/isEditModeOn", bState);
        },
    });
});

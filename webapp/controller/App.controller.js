sap.ui.define([
    "sap/ui/core/mvc/Controller"
 ], function (Controller) {
    "use strict";
    return Controller.extend("iba.gomel.by.myshop.controller.App", {});
 });

sap.ui.define([
    "../formatter/totalPriceFormatter",
    "sap/m/MessageToast",
    "./BaseController",
    "sap/m/MessageBox",
    "sap/ui/core/Fragment",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/BindingMode",
    "sap/ui/core/ValueState"
], function (totalPriceFormatter, MessageToast, BaseController, MessageBox, Fragment, JSONModel, BindingMode, ValueState) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.Card", {
        formatter: totalPriceFormatter,

        _onObjectMatched: function(oEvent) {
            var oParams = {
                sExpandParameters : "Currency,Card,Card/CardDetails,Card/CardDetails/Card,Card/CardDetails/Detail,Card/CardDetails/Detail/StockInfo",
                sModelName : "goods",
                bIsDeepInto : true               
            }
            this.setUserModel(oParams);
            oParams = {
                sExpandParameters : "Currency",
                sModelName : "personOdata",
                bIsDeepInto : false               
            }
            this.setUserModel(oParams);
        },
        
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("card").attachPatternMatched(this._onObjectMatched, this);
        },

        onCountChange: function(oEvent) {
            var oStepInput = oEvent.getSource();
            if (oStepInput.getValueState() != ValueState.None) {
                oStepInput.setValue(oStepInput.getMin());
                this.showErrorPopupMessage(this.getBundle().getText("stepInputOurOfRange"));
                oStepInput.setValueState("None");
            } else {
                var oController = this;
                var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];
                var oDataModel = this.getODataModel();
                oController.updateEntityWithBusy(oDataModel, "CardDetails", oCardDetail, "countHaveBeenChanged", oStepInput);              
            }
        },

        removeAll: function(oEvent) {
            var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];
            var oDataModel = this.getODataModel();
            this.removeEntityWithBusy(oDataModel, "CardDetails", oCardDetail.ID, "productsHasBeenRemoved", oEvent.getSource());
            this._onObjectMatched();
        },

        cleanUp: function(oEvent) {
            var oButton = oEvent.getSource();
            var oController = this;
            var aGoods = this.getModelByName("goods").getData();
            if (aGoods.length) {
                oButton.setBusy(true);

                var oDataModel = this.getODataModel();
                oDataModel.setDefaultBindingMode(BindingMode.TwoWay);
                oDataModel.setUseBatch(true);
                oDataModel.setDeferredGroups(["cleanUP"]);

                var iLength = aGoods.length;
                for (var i = 0; i < iLength; i++) {
                    oDataModel.remove("/CardDetails(" + aGoods[i].ID + ")", {});
                }

                oDataModel.submitChanges({
                    groupid: "cleanUP",
                    success: function () {
                        MessageToast.show(oController.getBundle().getText("successCleanUp"));
                        oButton.setBusy(false);
                    },
                    error: function (oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                        oButton.setBusy(false);
                    }
                });
            }
            this._onObjectMatched();
        },

        recountMoneyAndCount: function(oDataModel, aGoods) {
            var iSumm = 0;
            var iLength = aGoods.length;
            for (var i = 0; i < iLength; i++) {
                if (aGoods[i].IsBuyNow) {
                    iSumm += aGoods[i].Detail.Price * aGoods[i].Count;
                    var oStockModel = {};
                    oStockModel.ID = aGoods[i].Detail.StockInfo.ID;
                    oStockModel.Count = aGoods[i].Detail.StockInfo.Count - aGoods[i].Count;
                    oDataModel.update("/StockInfos(" + oStockModel.ID + ")", oStockModel, {method: "PUT"});
                    oDataModel.remove("/CardDetails(" + aGoods[i].ID + ")", {});
                }
            }
            return iSumm;
        },

        updateUserBalance: function(oDataModel, oUser, iSumm) {
            var oUserRecount = this.copyObject(oUser);
            oUserRecount.Balance = oUserRecount.Balance - iSumm;
            oDataModel.update("/Users(" + oUserRecount.ID + ")", oUserRecount, {method: "PUT"});            
        },

        checkOut: function(oEvent) {
            var oButton = oEvent.getSource();
            oButton.setBusy(true);
            MessageBox.confirm(this.getBundle().getText("cardCheckoutQuestion"), { 
                onClose: function(sAction) {
                    switch (sAction) {
                        case MessageBox.Action.OK:
                            var oController = this;
                            var aGoods = this.getModelByName("goods").getData();
                            var oUser = this.getModelByName("personOdata").getData();
                            if ((oUser.Balance * oUser.Currency.Ratio) >= this.getTotalSumm(aGoods, oUser.Currency)) {
                                oButton.setBusy(true);

                                var oDataModel = this.getODataModel();
                                oDataModel.setDefaultBindingMode(BindingMode.TwoWay);
                                oDataModel.setUseBatch(true);
                                oDataModel.setDeferredGroups(["checkout"]);

                                var iSumm = this.recountMoneyAndCount(oDataModel, aGoods);
                                this.updateUserBalance(oDataModel, oUser, iSumm)

                                oDataModel.submitChanges({
                                    groupid: "checkout",
                                    success: function () {
                                        MessageToast.show(oController.getBundle().getText("successCheckout"));
                                        oButton.setBusy(false);
                                    },
                                    error: function (oErr) {
                                        oController.showErrorPopupMessage(oErr.message);
                                        oButton.setBusy(false);
                                    }
                                });
                            } else {
                                MessageToast.show(this.getBundle().getText("notEnoughMoney"));
                            }
                            this._onObjectMatched();                            
                            break;
                        case MessageBox.Action.CANCEL:
                            break;
                        default:
                            break;
                    }
                }.bind(this)
            });
            oButton.setBusy(false);
        },

        showMoreInfo: function(oEvent) {
            var oView = this.getView();
            var oButton = oEvent.getSource();
            var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];

            var oData = this.prepareModel(oCardDetail)
            var oModel = new JSONModel(oData);
            this.getView().setModel(oModel, "pages");

            if (!this._oQuickView) {
                Fragment.load({
                    name: "iba.gomel.by.myshop.fragment.QuickView",
                    controller: this
                }).then(function (oQuickView) {
                    this._oQuickView = oQuickView;
                    oView.addDependent(this._oQuickView);
                    this._oQuickView.openBy(oButton);
                }.bind(this));
            } else {
                oView.addDependent(this._oQuickView);
                this._oQuickView.openBy(oButton);
            }
        },

        onSelect: function (oEvent) {
            var oCheckBox = oEvent.getSource();
            var oController = this;
            var oCardDetail = this.getModelByName("goods").getData()[oEvent.getSource().getBindingContext("goods").getPath().slice(1)];
            var oDataModel = this.getODataModel();
            oController.updateEntityWithBusy(oDataModel, "CardDetails", oCardDetail, "isBuyNowChanged", oCheckBox);

            this._onObjectMatched();
        },

        getTotalSumm: function (aGoods, oCurrency) {
            var iSumm = 0;
            var iLength = aGoods.length;
            for (var i = 0; i < iLength; i++) {
                iSumm += aGoods[i].Count * aGoods[i].Detail.Price;
            }
            return iSumm * oCurrency.Ratio;
        },

        /**
        * This is a function.
        * Return an array with one object. This object contains additional information about product to be shown in QuickView
        * @param {object} oCardDetail - A string param
        * @return {object} An array with one object
        */
        prepareModel: function (oCardDetail) {
            var aModels = [
                {
                    "productName": oCardDetail.Detail.ProductName,
                    "image": oCardDetail.Detail.ImageSrc,
                    "type": oCardDetail.Detail.Type,
                    "description": oCardDetail.Detail.Description,
                    "country": oCardDetail.Detail.Country,
                    "manufacturer": oCardDetail.Detail.Manufacturer,
                    "seller": oCardDetail.Detail.SellerName,
                    "email": oCardDetail.Detail.SellerEmail,
                    "phone": oCardDetail.Detail.SellerPhone,
                    "quantity": oCardDetail.Detail.StockInfo.Count                    
                }
            ];

            return aModels;
        }
    });
});

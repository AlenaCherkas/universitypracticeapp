sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/f/LayoutType"
 ], function (Controller, History, LayoutType) {
    "use strict";
    return Controller.extend("iba.gomel.by.myshop.controller.Menu", {

        onInit: function(){
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        },

        navigateTo: function(sRoute) {
            this.getOwnerComponent().getRootControl().byId("fcl").setLayout(LayoutType.OneColumn)
            this.oRouter.navTo(sRoute);
        },

        onNavigateBack: function() {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                this.navigateTo("home");
            }
        },

        onChangeLang: function(oEvent) {
            sap.ui.getCore().getConfiguration().setLanguage(oEvent.getSource().getSelectedKey());
        },

        onNavigateToCard: function() {
            this.navigateTo("card");
        },

        onNavigateToUserInfo: function() {
            this.navigateTo("userinfo");
        },

        onNavigateToHome: function() {
            this.navigateTo("home");
        },

        onNavigateToChosen: function() {
            this.navigateTo("chosen");
        }
    });
 });

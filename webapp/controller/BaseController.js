sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button",
    "sap/m/ButtonType",
    "sap/ui/model/resource/ResourceModel"
], function (Controller, JSONModel, MessageToast, Dialog, Text, Button, ButtonType, ResourceModel) {
    "use strict";

    return Controller.extend("iba.gomel.by.myshop.controller.BaseController", {

        getODataModel: function () {
            return this.getView().getModel("oDataModel");
        },

        getBundle: function () {
            return this.getModelByName("i18n").getResourceBundle();
        },

        getErrorMessage: function(sErrorText) {
            var sResultTextError = "";
            var oController = this;
            if (sErrorText === undefined) {
                sResultTextError = oController.getBundle().getText("errorUndefinedErrorText")
            } else {
                sResultTextError = sErrorText;
            }
            return oController.getBundle().getText("errorText", [sResultTextError]);
        },

        getModelByName: function(sName) {
            return this.getView().getModel(sName);
        },

        setModel: function(oModel, sName) {
            return this.getView().setModel(oModel, sName);
        },

        getModelProperty: function(sName, sProperty) {
            return this.getView().getModel(sName).getProperty(sProperty);
        },

        setModelProperty: function(sName, sProperty, oData) {
            return this.getView().getModel(sName).setProperty(sProperty, oData);
        },

        setUserModel: function(oParams) {
            var oDataModel = this.getODataModel();
            var oController = this;
            oDataModel.read("/Users", {
                urlParameters: {
                    $expand: oParams.sExpandParameters
                },
                success: function (oData, oResponse) {
                    var oModel = new JSONModel();
                    if (oParams.bIsDeepInto) {
                        oModel.setData(oData.results[0].Card.CardDetails.results);
                    } else if (oParams.bIsDeepInto == false) {
                        oModel.setData(oData.results[0]);
                    } else {
                        oModel.setData(oData.results[0].Card.ChosenDetails.results);
                    }
                    oController.setModel(oModel, oParams.sModelName);
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        },

        setCurrencies: function() {
            var oDataModel = this.getODataModel();
            var oController = this;
            oDataModel.read("/Currencies", {
                success: function (oData, oResponse) {
                    var oModel = new JSONModel();
                    oModel.setData(oData.results);
                    oController.setModel(oModel, "currencies");
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        },

        copyObject: function(oObject) {
            var oNewObject = {};
            Object.assign(oNewObject, oObject);
            return oNewObject;
        },

        createEntity: function(oDataModel, sEntityName, oNewEntity, sSuccessMsg) {
            var oController = this;
            oDataModel.create("/" + sEntityName, oNewEntity, {
                method: "POST",
                success: function (data) {
                    MessageToast.show(oController.getBundle().getText(sSuccessMsg));
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        },

        updateEntity: function(oDataModel, sEntityName, oNewEntity, sSuccessMsg) {
            var oController = this;
            oDataModel.update("/" + sEntityName + "(" + oNewEntity.ID +")", oNewEntity, {
                method: "PUT",
                success: function() {
                    MessageToast.show(oController.getBundle().getText(sSuccessMsg));
                },
                error: function(oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            })
        },

        removeEntity: function(oDataModel, sEntityName, iId, sSuccessMsg) {
            var oController = this;
            oDataModel.remove("/" + sEntityName + "(" + iId + ")", {
                success: function () {
                    MessageToast.show(oController.getBundle().getText(sSuccessMsg));
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                }
            });
        },

        removeEntityWithBusy: function(oDataModel, sEntityName, iId, sSuccessMsg, oControl) {
            var oController = this;
            oControl.setBusy(true);
            oDataModel.remove("/" + sEntityName + "(" + iId + ")", {
                success: function () {
                    MessageToast.show(oController.getBundle().getText(sSuccessMsg));
                    oControl.setBusy(false);
                },
                error: function (oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                    oControl.setBusy(false);
                }
            });
        },

        updateEntityWithBusy: function(oDataModel, sEntityName, oNewEntity, sSuccessMsg, oControl) {
            var oController = this;
            oControl.setBusy(true);
            oDataModel.update("/" + sEntityName + "(" + oNewEntity.ID +")", oNewEntity, {
                method: "PUT",
                success: function() {
                    MessageToast.show(oController.getBundle().getText(sSuccessMsg));
                    oControl.setBusy(false);
                },
                error: function(oErr) {
                    oController.showErrorPopupMessage(oErr.message);
                    oControl.setBusy(false);
                }
            })
        },

        showErrorPopupMessage: function(sMessage) {
            var oDialog = new Dialog({
                title: this.getBundle().getText("errorPopupTitle"),
                type: 'Message',
                state: 'Error',
                content: new Text({
                    text: sMessage
                }),
                beginButton: new Button({
                    type: ButtonType.Emphasized,
                    text: this.getBundle().getText("errorButtonPopupText"),
                    press: function () {
                        oDialog.close();
                    }
                }),
                afterClose: function () {
                    oDialog.destroy();
                }
            });

            oDialog.open();
        }
    });
});

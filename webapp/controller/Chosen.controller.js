sap.ui.define([
    "./BaseController",
    "sap/m/MessageToast",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
], function (BaseController, MessageToast, Filter, FilterOperator) {
    "use strict";

    return BaseController.extend("iba.gomel.by.myshop.controller.Chosen", {
        _onObjectMatched: function (oEvent) {
            var oParams = {
                sExpandParameters : "Currency,Card,Card/ChosenDetails,Card/ChosenDetails/Card,Card/ChosenDetails/Detail,Card/ChosenDetails/Detail/StockInfo",
                sModelName : "chosens",
                bIsDeepInto : undefined               
            }
            this.setUserModel(oParams);
            oParams = {
                sExpandParameters : "Currency",
                sModelName : "personOdata",
                bIsDeepInto : false               
            }
            this.setUserModel(oParams);
        },

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("chosen").attachPatternMatched(this._onObjectMatched, this);
        },

        removeChosen: function (oEvent) {
            var oChosenDetail = this.getModelByName("chosens").getData()[oEvent.getSource().getBindingContext("chosens").getPath().slice(1)];
            var oDataModel = this.getODataModel();
            this.removeEntity(oDataModel, "ChosenDetails", oChosenDetail.ID, "chosenProductHasBeenRemoved");
            this._onObjectMatched();
        },

        addToCard: function (oEvent) {
            var oController = this;
            var oChosenDetail = this.getModelByName("chosens").getData()[oEvent.getSource().getBindingContext("chosens").getPath().slice(1)];
            var oDataModel = this.getODataModel();

            oDataModel.read("/CardDetails", {
                filters: [
                    new Filter({
                        path: "DetailID",
                        operator: FilterOperator.EQ,
                        value1: oChosenDetail.DetailID
                    }),
                    new Filter({
                        path: "CardID",
                        operator: FilterOperator.EQ,
                        value1: oController.getModelProperty("personOdata", "/CardID")
                    })
                ],
                success: function (oData, oResponse) {
                    if (oData.results.length === 0) {
                        var oNewCardDetail = {
                            "CardID": oController.getModelProperty("personOdata", "/CardID"),
                            "DetailID": oChosenDetail.DetailID,
                            "Count": 1,
                            "IsBuyNow": true
                        }
                        oController.createEntity(oDataModel, "CardDetails", oNewCardDetail, "productHasBeenAdded");
                    } else {
                            MessageToast.show(oController.getBundle().getText("productHasBeenAddedBefore"));
                        }
                    },
                    error: function (oErr) {
                        oController.showErrorPopupMessage(oErr.message);
                    }
                });

            this._onObjectMatched();
        },
    });
});

sap.ui.define([], function() {
    "use strict";
    return {
        totalPrice: function(aDetailsList, oCurrency) {
            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var iSumm = 0;
            var iLength = aDetailsList.length;

            for (var i = 0; i < iLength; i++) {
                if (aDetailsList[i].IsBuyNow) {
                    iSumm += aDetailsList[i].Count * aDetailsList[i].Detail.Price;
                }
            }

            if (iSumm != 0) {
                return oBundle.getText("notEmptyCardText", [iSumm * oCurrency.Ratio, oCurrency.Name]);                
            } else {
                return oBundle.getText("epmtyCardText");                
            }
        }
    }
});

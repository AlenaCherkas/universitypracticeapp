sap.ui.define([], function () {
    "use strict";

    var oCurrencyRenderer = {
        apiVersion: 2
    };

    oCurrencyRenderer.render = function (oRm, oObjStatus) {
        oRm.openStart("div", oObjStatus);

        if (oObjStatus._isEmpty()) {
            oRm.style("display", "none");
            oRm.openEnd();
        } else {
            if (oObjStatus.getCurr()) {
                var sCurrencyValue = oObjStatus.getCurr();
                oRm.openStart("span", oObjStatus.getId() + "-text");
                oRm.class("style" + sCurrencyValue);
                oRm.openEnd();
                oRm.text(sCurrencyValue);
                oRm.close("span");
            }
        }
        oRm.close("div");
    };

    return oCurrencyRenderer;
}, true);

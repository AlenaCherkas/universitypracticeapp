sap.ui.define([
    "sap/m/ObjectStatus"
], function (ObjectStatus) {
    "use strict";

    var CurrencyObjectStatus = ObjectStatus.extend("iba.gomel.by.myshop.control.CurrencyObjectStatus", {
        metadata: {
            properties: {
                curr: { type: "string", group: "Misc", defaultValue: null },
            }
        }
    });

    CurrencyObjectStatus.prototype.setCurr = function (sValue) {
        return this.setProperty("curr", sValue);
    }
     
    return CurrencyObjectStatus;
});
